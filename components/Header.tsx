import React from "react";
// import Image from "next/image";

export default function Header() {
  return (
    <div className="relative pr-3">
       <style jsx>{`
       
        text {
          background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
          -webkit-text-fill-color: transparent;
          -webkit-background-clip: text;
          /* Below is not needed */
          display:block;
          font-size: 200px;
          text-align: center;
          margin-top: 20px;
          margin-left: 2rem;
          font-weight: bold;
          letter-spacing: 1px;
          background-size: 400% 400%;
          animation: gradient 10s ease infinite;
          font-variation-settings: 'wght' 900;

        }
        .backtext{
          background-color: black;
          width: 90rem;
        }
        @media (max-width: 1280px) {
          text {
            background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
            -webkit-text-fill-color: transparent;
            -webkit-background-clip: text;
            /* Below is not needed */
            display:block;
            font-size: 160px;
            text-align: center;
            margin-top: 20px;
            font-weight: bold;
            letter-spacing: 1px;
            background-size: 400% 400%;
            animation: gradient 10s ease infinite;
            font-variation-settings: 'wght' 900;
            
          }
          .backtext{
            background-color: black;
            width: auto;
          }
          }
        @media (max-width: 1024px) {
          text {
            background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
            -webkit-text-fill-color: transparent;
            -webkit-background-clip: text;
            /* Below is not needed */
            display:block;
            font-size: 130px;
            text-align: center;
            margin-top: 20px;
            font-weight: bold;
            letter-spacing: 1px;
            background-size: 400% 400%;
            animation: gradient 10s ease infinite;
            font-variation-settings: 'wght' 900;
  
          }
          .backtext{
            background-color: black;
            width: auto;
          }
          }
          @media (max-width: 900px) {
            text {
              background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
              -webkit-text-fill-color: transparent;
              -webkit-background-clip: text;
              /* Below is not needed */
              display:block;
              font-size: 110px;
              text-align: center;
              margin-top: 20px;
              font-weight: bold;
              letter-spacing: 1px;
              background-size: 400% 400%;
              animation: gradient 10s ease infinite;
              font-variation-settings: 'wght' 900;
              
            }
            .backtext{
              background-color: black;
              width: auto;
            }
            }
          @media (max-width: 780px) {
            text {
              background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
              -webkit-text-fill-color: transparent;
              -webkit-background-clip: text;
              /* Below is not needed */
              display:block;
              font-size: 85px;
              text-align: center;
              margin-top: 20px;
              font-weight: bold;
              letter-spacing: 1px;
              background-size: 400% 400%;
              animation: gradient 10s ease infinite;
              font-variation-settings: 'wght' 900;
              
            }
            .backtext{
              background-color: black;
              width: auto;
            }
            }
            @media (max-width: 604px) {
              text {
                background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
                -webkit-text-fill-color: transparent;
                -webkit-background-clip: text;
                /* Below is not needed */
                display:block;
                font-size:55px;
                text-align: center;
                margin-top: 20px;
                font-weight: bold;
                letter-spacing: 1px;
                background-size: 400% 400%;
                animation: gradient 10s ease infinite;
                font-variation-settings: 'wght' 900;
                
              }
              .backtext{
                background-color: black;
                width: auto;
              }
              }
              @media (max-width: 430px) {
                text {
                  background: linear-gradient(-45deg, #fbd76b, #f9484a, #00bbf9);
                  -webkit-text-fill-color: transparent;
                  -webkit-background-clip: text;
                  /* Below is not needed */
                  display:block;
                  font-size: 48px;
                  text-align: center;
                  margin-top: 20px;
                  font-weight: bold;
                  letter-spacing: 1px;
                  background-size: 400% 400%;
                  animation: gradient 10s ease infinite;
                  font-variation-settings: 'wght' 900;
                  
                }
                .backtext{
                  background-color: black;
                  width: auto;
                }
                }
      `}</style>
      <div className="absolute inset-2 animate-gradient opacity-100"></div>
      <div className="relative">
        <div className="px-2">
          <div className="backtext">
          <text >
            I Build Things
          </text>
          </div>
        </div>
      </div>
    </div>
  );
}
